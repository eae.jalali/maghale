package com.example.maghale.user.service;

import com.example.maghale.user.controller.UserModel;

public interface UserService {
    public UserModel index();
}
