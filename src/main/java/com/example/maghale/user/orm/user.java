package com.example.maghale.user.orm;

import com.example.maghale.cofiguration.Runconfiguration;
import com.example.maghale.maghale.orm.maghale;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name="user" ,schema= Runconfiguration.DB)
@Data
public class user {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="statusAccount")
    private boolean Status;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    Collection<maghale> maghaleList;


}
