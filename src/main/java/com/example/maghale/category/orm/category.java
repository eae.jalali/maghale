package com.example.maghale.category.orm;

import com.example.maghale.cofiguration.Runconfiguration;
import com.example.maghale.maghale.orm.maghale;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table (name="Category" , schema = Runconfiguration.DB)
@Data
public class category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name="name")
    private String name;

    @OneToMany(mappedBy = "category",fetch = FetchType.LAZY)
    Collection<maghale> maghaleList;

}
