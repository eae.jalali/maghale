package com.example.maghale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaghaleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaghaleApplication.class, args);
    }

}
