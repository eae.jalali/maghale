package com.example.maghale.maghale.orm;

import com.example.maghale.category.orm.category;
import com.example.maghale.cofiguration.Runconfiguration;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="maghale" ,schema = Runconfiguration.DB)
@Data
public class maghale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="name")
    private String name;

    @Column (name ="status_maghale")
    private boolean StatusMaghale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    private com.example.maghale.user.orm.user user;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="category_id",referencedColumnName = "id")
    private category category;

}
